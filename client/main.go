package main

import (
	"context"
	"crypto/tls"
	"io"
	"math/rand"
	"time"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/assembly4/bern"
	pb "gitlab.com/assembly4/proto/zug"
)

const (
	domain   = "assembly4.tk"
	address  = "zug." + domain + ":443"
	loginUrl = "http://geneva." + domain + "/api/v1/login"
)

var (
	opts []grpc.DialOption
	c    pb.RedisClient
)

// publish to uuid channel every seconds
func pub(uuid string, seconds int) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(45*time.Second))
	defer cancel()

	for i := 0; i < 20; i++ {
		lat := rand.Intn(360) - 180
		long := rand.Intn(360) - 180

		start := time.Now()
		r, err := c.PublishDeviceLoc(ctx, &pb.Location{
			DeviceId: uuid,
			Time:     "2021-08-27T12:10:50Z",
			Lat:      float32(lat),
			Long:     float32(long),
		})
		if err != nil {
			log.Fatal().Msgf("could not publish loc: %v", err)
		}
		elapsed := time.Since(start)
		log.Info().Msgf("| %s | %s ", elapsed, r.GetStatus())

		time.Sleep(time.Duration(seconds) * time.Second)
	}
}

// stream to uuid channel every <ms>
func stream(token string, uuid string, ms int) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(25*time.Second))
	defer cancel()

	stream, err := c.StreamDeviceLoc(ctx)
	if err != nil {
		log.Fatal().Msgf("%v.streamDeviceLoc(_) = _, %v", c, err)
	}

	// setup options
	stream.Send(&pb.LocationWithSetup{
		Content: &pb.LocationWithSetup_Options{
			Options: &pb.Options{
				DeviceId: uuid,
				Token:    token,
			},
		},
	})

	for i := 0; i < 100; i++ {
		lat := rand.Intn(360) - 180
		long := rand.Intn(360) - 180

		start := time.Now()
		if err := stream.Send(&pb.LocationWithSetup{
			Content: &pb.LocationWithSetup_Location{
				Location: &pb.Location{
					DeviceId: uuid,
					Time:     "2021-08-27T12:10:50Z",
					Lat:      float32(lat),
					Long:     float32(long),
				},
			},
		}); err != nil {
			log.Fatal().Msgf("%v.streamDeviceLoc(_) = _, %v", c, err)
		}
		elapsed := time.Since(start)
		log.Info().Int("i", i).Dur("elapsed", elapsed).Msg("injecting into stream")
		time.Sleep(time.Duration(ms) * time.Millisecond)
	}

	reply, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatal().Msgf("%v.CloseAndRecv() got error %v, want %v", stream, err, nil)
	}
	log.Printf("Publish stream reply: %v", reply)
}

// subscribe to multiple uuids channels
func sub(token string, uuids []string) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2*time.Minute))
	defer cancel()

	devices := pb.Devices{
		Token: token,
		Uuids: uuids,
	}

	stream, err := c.SubscribeToDevices(ctx)
	if err != nil {
		log.Fatal().Msgf("%v.SubscribeToDevices(_) = _, %v", c, err)
	}
	if err := stream.Send(&devices); err != nil {
		log.Fatal().Msgf("%v.SubscribeToDevices(_) = _, %v", c, err)
	}
	//go func ()  {
	//	time.Sleep(2*time.Second)
	//	uuids = nil
	//	uuids = append(uuids, "3c005735-3938-4370-b2b1-8a98f28500b9")
	//	devices.Uuids = uuids
	//	devices.Token = token
	//	if err := stream.Send(&devices); err != nil {
	//		log.Fatalf("%v.SubscribeToDevices(_) = _, %v", c, err)
	//	}
	//}()

	for {
		loc, err := stream.Recv()
		if err == io.EOF {
			log.Warn().Msg("end of stream")
			break
		}
		if err != nil {
			log.Fatal().Msgf("%v.SubscribeToDevices(_) = _, %v", c, err)
		}
		log.Info().Float32("lat", loc.Lat).Float32("long", loc.Long).Str("deviceId", loc.DeviceId).Send()
	}
}

func main() {
	bern.SetupLogger()

	tc := credentials.NewTLS(&tls.Config{InsecureSkipVerify: true})
	opts = append(opts, grpc.WithTransportCredentials(tc))

	conn, err := grpc.Dial(address, opts...)
	if err != nil {
		log.Fatal().Msgf("did not connect: %v", err)
	}
	c = pb.NewRedisClient(conn)

	token, _ := bern.Login(loginUrl, "user1@domain.com", "superpass1")

	// subscribe to device channel
	go sub(token, []string{"6c05b52e-d239-4422-b514-4f522012e3fd", "591a45bf-8d71-475c-b9dd-9750aeba8e21"})

	// stream (publish) directly to device channel
	//go stream(token, "02991a5d-e5ab-4b8c-882a-3263fff30915", 1)

	// publish directly to device channel (only internal traffic)
	//go pub("c11b7a42-e4db-4b5b-95b1-76751260b845", 2)

	time.Sleep(3 * time.Minute)
}
