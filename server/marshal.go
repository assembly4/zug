package main

import (
	"encoding/json"
	"time"

	"github.com/rs/zerolog/log"
	pb "gitlab.com/assembly4/proto/zug"
)

type marshalledLoc struct {
	Uuid string    `validate:"required,uuid4" json:"device_id"`
	Time time.Time `validate:"required,lte" json:"time"`
	Lat  float32   `validate:"gte=-180,lte=180" json:"lat"`
	Long float32   `validate:"gte=-180,lte=180" json:"long"`
}

func marshal(l pb.Location) ([]byte, error) {
	t, err := time.Parse(time.RFC3339, l.Time)
	if err != nil {
		log.Error().Err(err).Send()
		return nil, err
	}

	loc := marshalledLoc{
		Uuid: l.DeviceId,
		Time: t,
		Lat:  l.Lat,
		Long: l.Long,
	}

	if err := validate.Struct(loc); err != nil {
		log.Error().Err(err).Send()
		return nil, err
	}

	location, err := json.Marshal(loc)
	if err != nil {
		log.Error().Err(err).Send()
		return nil, err
	}

	return location, nil
}
