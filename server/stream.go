package main

import (
	"io"

	"github.com/rs/zerolog/log"
	"gitlab.com/assembly4/bern"
	pb "gitlab.com/assembly4/proto/zug"
)

func (s *RedisServer) StreamDeviceLoc(stream pb.Redis_StreamDeviceLocServer) error {
	defer log.Debug().Msgf("exiting %s", bern.GetFunctionName())

	var (
		allowedDevices = make(map[string]bool)
	)

	// listen on stream
	for {
		incoming, err := stream.Recv()
		if err == io.EOF {
			log.Warn().Msg("end of stream")
			return err
		}
		if err != nil {
			log.Error().Err(err).Send()
			return err
		}

		switch incoming.Content.(type) {
		case *pb.LocationWithSetup_Options:
			log.Debug().Msg("processing options")
			// check if authorized for device
			x := map[string]bool{incoming.GetOptions().DeviceId: true}
			allowedDevices, err = bern.Authorized(incoming.GetOptions().Token, x, true, publicKey)
			if err != nil {
				log.Error().Err(err).Send()
				return stream.SendAndClose(&pb.Status{Status: err.Error()})
			}
			if len(allowedDevices) != 1 {
				log.Error().Msg("not authorized to write for the device")
				return stream.SendAndClose(&pb.Status{Status: "not authorized to write for the device"})
			}
		case *pb.LocationWithSetup_Location:
			log.Debug().Msg("processing location")
			if _, ok := allowedDevices[incoming.GetLocation().DeviceId]; !ok {
				log.Error().Msg("not authorized to write to the device")
				return stream.SendAndClose(&pb.Status{Status: "not authorized to write to the device"})
			}

			message, err := marshal(*incoming.GetLocation())
			if err != nil {
				log.Error().Err(err).Send()
				return err
			}

			rdb.Publish(incoming.GetLocation().DeviceId, message)
		}
	}
}
