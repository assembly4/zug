package main

import (
	"context"

	"github.com/rs/zerolog/log"
	"gitlab.com/assembly4/bern"
	pb "gitlab.com/assembly4/proto/zug"
)

// without auth as it is internal function
func (s *RedisServer) PublishDeviceLoc(ctx context.Context, in_loc *pb.Location) (*pb.Status, error) {
	defer log.Debug().Msgf("exiting %s", bern.GetFunctionName())

	message, err := marshal(*in_loc)
	if err != nil {
		log.Error().Err(err).Send()
		return &pb.Status{Status: "error"}, err
	}
	log.Debug().Msg("publishing loc to redis")
	rdb.Publish(in_loc.DeviceId, message)

	return &pb.Status{Status: "okay"}, nil
}
