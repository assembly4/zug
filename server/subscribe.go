package main

import (
	"context"
	"encoding/json"
	"reflect"
	"time"

	"github.com/rs/zerolog/log"
	"golang.org/x/sync/errgroup"

	"gitlab.com/assembly4/bern"
	pb "gitlab.com/assembly4/proto/zug"
)

func (s *RedisServer) SubscribeToDevices(stream pb.Redis_SubscribeToDevicesServer) error {
	defer log.Debug().Msgf("exiting %s", bern.GetFunctionName())

	var authCh = make(chan map[string]bool)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	g, ctx := errgroup.WithContext(ctx)

	// listen on client devices stream
	g.Go(func() error { return clientDevices(ctx, stream, authCh) })
	// periodically check context health
	g.Go(func() error { return subscribeHealthCheck(ctx, stream) })
	// listen on pub/sub and stream to client
	g.Go(func() error { return subscribeStream(ctx, stream, authCh) })

	if err := g.Wait(); err != nil {
		log.Error().Err(err)
	}

	return nil
}

func clientDevices(ctx context.Context, stream pb.Redis_SubscribeToDevicesServer, authCh chan map[string]bool) error {
	defer log.Debug().Msgf("exiting %s", bern.GetFunctionName())

	var (
		token            string
		requestedDevices = make(map[string]bool)
	)

	go func() {
		defer log.Debug().Msgf("exiting %s", bern.GetFunctionName())
		for {
			select {
			case <-ctx.Done():
				return
			default:
				time.Sleep(3 * time.Second)
				authorizedDevices, err := bern.Authorized(token, requestedDevices, false, publicKey)
				if err != nil {
					log.Error().Err(err)
					return
				}
				log.Debug().Msg("authCh <- authorizedDevices")
				authCh <- authorizedDevices
			}
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			incoming, err := stream.Recv()
			if err != nil {
				return err
			}

			if err := validate.Var(incoming.Uuids, "required,max=10,unique,dive,uuid4"); err != nil {
				return err
			}
			log.Info().Strs("requestedDevices", incoming.Uuids).Send()

			requestedDevices = make(map[string]bool)
			for _, device := range incoming.Uuids {
				requestedDevices[device] = true
			}

			token = incoming.Token

			authorizedDevices, err := bern.Authorized(incoming.Token, requestedDevices, false, publicKey)
			if err != nil {
				return err
			}
			log.Debug().Msgf("authorizedDevices: %v", authorizedDevices)
			authCh <- authorizedDevices
		}
	}
}

func subscribeStream(ctx context.Context, stream pb.Redis_SubscribeToDevicesServer, authCh chan map[string]bool) error {
	defer log.Debug().Msgf("exiting %s", bern.GetFunctionName())

	var (
		oldDevices    = make(map[string]bool)
		subscriptions []string
		subs          = rdb.Subscribe()
	)

	defer subs.Close()
	ch := subs.Channel()

	for {
		select {
		case <-ctx.Done():
			//dispose before exiting so the sender is not left in a deadlock state
			<-authCh
			return nil
		case newDevices := <-authCh:
			log.Debug().Msgf("newDevices: %v, oldDevices: %v", newDevices, oldDevices)
			if !reflect.DeepEqual(oldDevices, newDevices) {
				subs.Unsubscribe()
				subscriptions = nil
				for k := range newDevices {
					subscriptions = append(subscriptions, k)
				}
				log.Debug().Msgf("subscriptions: %v", subscriptions)
				subs.Subscribe(subscriptions...)
				oldDevices = newDevices
			}
		case msg := <-ch:
			log.Info().Msg("new message from channel")
			ml := pb.Location{}
			json.Unmarshal([]byte(msg.Payload), &ml)
			if err := stream.Send(&pb.Location{
				DeviceId: ml.DeviceId,
				Time:     ml.Time,
				Lat:      ml.Lat,
				Long:     ml.Long,
			}); err != nil {
				return err
			}
		}
	}
}

func subscribeHealthCheck(ctx context.Context, stream pb.Redis_SubscribeToDevicesServer) error {
	defer log.Debug().Msgf("exiting %s", bern.GetFunctionName())

	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			time.Sleep(5 * time.Second)
			if err := stream.Context().Err(); err != nil {
				return err
			}
		}
	}
}
