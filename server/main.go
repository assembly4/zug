package main

import (
	"crypto/rsa"
	"net"
	"os"

	"github.com/go-playground/validator"
	"github.com/go-redis/redis"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"

	"gitlab.com/assembly4/bern"
	pb "gitlab.com/assembly4/proto/zug"
)

var (
	publicKey *rsa.PublicKey
	rdb       *redis.ClusterClient
	validate  = validator.New()
)

const port = ":50124"

type RedisServer struct {
	pb.UnimplementedRedisServer
}

func main() {
	defer rdb.Close()

	var err error

	requiredEnv := []string{"PUBLIC_KEY", "LOG_LEVEL", "REDIS_HOST", "REDIS_PASS", "GENEVA_FQDN", "DEVICES_ENDPOINT"}
	bern.CheckEnvVars(requiredEnv)
	bern.SetupLogger()

	publicKey, err = bern.LoadPublicKey(os.Getenv("PUBLIC_KEY"))
	if err != nil {
		log.Fatal().Msgf("unable to load public key: %v", err)
	}

	rdb = redis.NewClusterClient(&redis.ClusterOptions{
		Addrs:    []string{os.Getenv("REDIS_HOST")},
		Password: os.Getenv("REDIS_PASS"), // no password set
	})

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Panic().Msgf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterRedisServer(s, &RedisServer{})
	log.Info().Msgf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Panic().Msgf("failed to serve: %v", err)
	}
}
