module assembly/zug

go 1.17

require (
	gitlab.com/assembly4/proto v0.0.0-20220115222106-8adf847eaa37
	google.golang.org/grpc v1.44.0
)

require (
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20220128215802-99c3d69c2c27 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20220201184016-50beb8ab5c44 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)

require (
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-gonic/gin v1.7.7 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator/v10 v10.10.0 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/onsi/ginkgo v1.16.4 // indirect
	github.com/onsi/gomega v1.16.0 // indirect
	github.com/ugorji/go/codec v1.2.6 // indirect
	golang.org/x/crypto v0.0.0-20220131195533-30dcbda58838 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/rs/zerolog v1.26.1
	gitlab.com/assembly4/bern v0.0.0-20220202084647-95fa7a1415de
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
